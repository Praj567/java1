package filehandling;

import java.io.File;



public class GetFileInfo {



   public static void main(String[] args) {
        
        File obj= new File("C:\\Users\\241359\\git\\java1\\basics\\src\\java.txt");
        System.out.println(obj.exists());
        
        if(obj.exists()) {
            System.out.println("File Name:"+obj.getName());
            System.out.println("File Path Address:"+obj.getAbsolutePath());
            System.out.println("File Writable or not:"+obj.canWrite());
            System.out.println("File Readable or not:"+obj.canRead());
            System.out.println("File Size in bytes:"+obj.length());
            
        }
    }



}