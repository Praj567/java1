package collections;

import java.util.List;
import java.util.ArrayList;


public class ListExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> list = new ArrayList<>();
		
		list.add("prajwal");
		list.add("nihal");
		list.add("chirag");
		list.add("prasad");
		list.add("shreya");
		System.out.println(list);
		int tsize = list.size();
		System.out.println(tsize);
		
		List<String> countries = new ArrayList<>();
		
		countries.add("India");
		countries.add("Australia");
		countries.add("Thailand");
		System.out.println(countries);
		
		int ssize = countries.size();
		System.out.println(ssize);

	}

}
