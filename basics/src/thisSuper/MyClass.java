package thisSuper;

class MyParent{
	String name = "This is parent class";
}

public class MyClass extends MyParent {
	String name = "This is instance variable";
	
	void somemethod() {
		String name= "This is local variable";
		System.out.println(name);
		System.out.println(this.name);
		System.out.println(super.name);
	
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyClass mc = new MyClass();
		mc.somemethod();
		

	}

}
